
import { SkElement } from '../../sk-core/src/sk-element.js';
import { DateLocale } from './locale/date-locale.js';

import { DATEPICKER_FORWARDED_ATTRS } from './impl/sk-datepicker-impl.js';

import { LANGS_BY_CODES } from '../../sk-core/src/sk-locale.js';

export const CALENDAR_NO_ICON_AN = "no-icon";

export class SkDatePicker extends SkElement {

    get cnSuffix() {
        return 'datepicker';
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }

    get fmt() {
        return this.getAttribute('fmt') || this.dateLocale ? this.dateLocale.shortDateFormat : 'm/d/Y';
    }

    get dateLocale() {
        if (! this._lang) {
            this._lang = this.lang ? this.localeByCode(this.lang) : DateLocale.EN;
        }
        return this._lang;
    }

    get value() {
        return this.getAttribute('value');
    }

    set value(value) {
        if (value === null) {
            this.removeAttribute('value');
        } else {
            this.setAttribute('value', value);
        }
    }

    localeByCode(langCode) {
        return DateLocale[LANGS_BY_CODES[langCode]];
    }

    get validity() {
        return this.inputEl ?  this.inputEl.validity : false;
    }

    get validationMessage() {
        return this.inputEl && this.inputEl.validationMessage
            && this.inputEl.validationMessage !== 'undefined' ? this.inputEl.validationMessage : this.locale.tr('Field value is invalid');
    }

    set validationMessage(validationMessage) {
        this._validationMessage = validationMessage;
    }

    set impl(impl) {
        this._impl = impl;
    }

    get subEls() {
        return [ 'inputEl' ];
    }

    get inputEl() {
        if (! this._inputEl && this.el) {
            this._inputEl = this.el.querySelector('input');
        }
        return this._inputEl;
    }

    set inputEl(el) {
        this._inputEl = el;
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (! this.inputEl) {
            return false;
        }
        if (DATEPICKER_FORWARDED_ATTRS[name] && oldValue !== newValue) {
            if (newValue !== null && newValue !== undefined) {
                this.inputEl.setAttribute(name, newValue);
            } else {
                this.inputEl.removeAttribute(name);
            }
        }
        if (name === 'form-invalid') {
            if (newValue !== null) {
                this.impl.showInvalid();
            } else {
                this.impl.showValid();
            }
        }
        if (name === 'value') {
            this.dispatchEvent(new CustomEvent('skchange', { target: this, detail: { value: this.getAttribute('value'), checked: this.getAttribute('checked') }, bubbles: true, composed: true }));
            this.dispatchEvent(new CustomEvent('change', { target: this, detail: { value: this.getAttribute('value'), checked: this.getAttribute('checked') }, bubbles: true, composed: true }));
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
    }

    setCustomValidity(validity) {
        super.setCustomValidity(validity);
        this.inputEl.setCustomValidity(validity);
    }

    static get observedAttributes() {
        return ['form-invalid', ...Object.keys(DATEPICKER_FORWARDED_ATTRS)];
    }

}
