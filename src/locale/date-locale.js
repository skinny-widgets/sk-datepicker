

import { DateLocale as DateLocaleBase } from '../../../dateutils/src/global.js';

import { RU } from './RU.js';

DateLocaleBase.RU = RU;

export const DateLocale = Object.assign({ }, DateLocaleBase);



