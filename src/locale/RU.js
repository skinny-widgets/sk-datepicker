

import { RU as RUBase } from '../../../dateutils/src/locale/RU.js';

RUBase.shortDateFormat = 'd.m.Y';
RUBase.weekDateFormat = 'D d.m.Y';
RUBase.dateTimeFormat = 'D d.m.Y G:i';

export const RU = RUBase;
