

import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';
import { SkRequired } from "../../../sk-form-validator/src/validators/sk-required.js";
import { SkMax } from "../../../sk-form-validator/src/validators/sk-max.js";
import { SkMin } from "../../../sk-form-validator/src/validators/sk-min.js";



export var DATEPICKER_FORWARDED_ATTRS = {
    'name': 'name', 'value': 'value',
    'disabled': 'disabled', 'required': 'required',
    'pattern': 'pattern',
    'min': 'min', 'max': 'max', 'step': 'step',
};


export class SkDatepickerImpl extends SkComponentImpl {

    get suffix() {
        return 'datepicker';
    }

    forwardAttributes() {
        for (let attrName of Object.keys(DATEPICKER_FORWARDED_ATTRS)) {
            let value = this.comp.getAttribute(attrName);
            if (value) {
                this.input.setAttribute(attrName, value);
            } else {
                if (this.comp.hasAttribute(attrName)) {
                    this.input.setAttribute(attrName, '');
                }
            }
        }
    }

    onInputChange(event) {
        this.comp.setAttribute('value', event.target.value);
        let result = this.validateWithAttrs();
        if (typeof result === 'string') {
            this.input.setCustomValidity(result);
            this.showInvalid();
        } else {
            this.showValid();
        }
        this.comp.dispatchEvent(new CustomEvent('change', { target: event.target, detail: { value: event.target.value, checked: event.target.checked }, bubbles: true, composed: true }));
        this.comp.dispatchEvent(new CustomEvent('skchange', { target: event.target, detail: { value: event.target.value, checked: event.target.checked }, bubbles: true, composed: true }));
    }

    onInputInput(event) {
        this.comp.setAttribute('value', event.target.value);
        let result = this.validateWithAttrs();
        if (typeof result === 'string') {
            this.input.setCustomValidity(result);
            this.showInvalid();
        } else {
            this.showValid();
        }
        this.comp.dispatchEvent(new CustomEvent('input', { target: event.target, detail: { value: event.target.value, checked: event.target.checked }, bubbles: true, composed: true }));
        this.comp.dispatchEvent(new CustomEvent('skinput', { target: event.target, detail: { value: event.target.value, checked: event.target.checked }, bubbles: true, composed: true }));
    }

    restoreState(state) {
        super.restoreState(state);
        this.clearAllElCache();
        this.forwardAttributes();
        this.bindChangeEvents();
    }

    bindChangeEvents() {
        if (this.input) {
            this.input.onchange = function(event) {
                this.comp.callPluginHook('onEventStart', event);
                this.onInputChange(event);
                this.comp.callPluginHook('onEventEnd', event);
            }.bind(this);
            this.input.oninput = function(event) {
                this.comp.callPluginHook('onEventStart', event);
                this.onInputInput(event);
                this.comp.callPluginHook('onEventEnd', event);
            }.bind(this);
            this.comp.addEventListener('skinput', (event) => {
                this.comp.callPluginHook('onEventStart', event);
                let result = this.validateWithAttrs();
                if (typeof result === 'string') {
                    this.input.setCustomValidity(result);
                    this.showInvalid();
                } else {
                    this.input.setCustomValidity('');
                    this.showValid();
                }
                this.comp.callPluginHook('onEventEnd', event);
            });
        }

    }

    get skValidators() {
        if (! this._skValidators) {
            this._skValidators = {
                'sk-required': new SkRequired(),
                'sk-min': new SkMin(),
                'sk-max': new SkMax(),
            };
        }
        return this._skValidators;
    }

    showInvalid() {
        super.showInvalid();
        this.input.classList.add('form-invalid');
    }

    showValid() {
        super.showValid();
        this.input.classList.remove('form-invalid');
    }

    focus() {
        setTimeout(() => {
            this.comp.inputEl.focus();
        }, 0);
    }
}